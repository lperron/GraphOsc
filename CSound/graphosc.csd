<CsoundSynthesizer>
<CsOptions>
; Select audio/midi flags here according to platform
-odac           -iadc    -+rtmidi=portmidi -Ma
;-odac           -iadc    -+rtmidi=virtual -M0
;-iadc    ;;;uncomment -iadc if realtime audio input is needed too
; For Non-realtime ouput leave only the line below:
; -o Cac40.wav -W ;;; for file output any platform
</CsOptions> 
<CsInstruments>

sr = 44100 
ksmps = 32 
0dbfs  = 1 
nchnls = 4

massign 1,1
prealloc 1,10  


; Instrument #1 - uses poscil3 for playing samples from a function table
instr 1
inote cpsmidi
iveloc ampmidi 1
idur = 0
    xtratim 1

kgate oscil 1,10,2
ifn = 2
iskip = p6
iatt =0.0001
idec=0.0001
islev=1
irel=0.001
kadsr madsr iatt, idec, islev, irel
kamp= kadsr*0.8

  
  a1 poscil3 kamp, inote, ifn
  out a1, a1


endin
</CsInstruments>
<CsScore>
f0 3600
;marqueur table
f1 0 32 27 1 1 2 14 5 103 6 109 7 89 8 107 9 84 12 -32 13 16 14 -40 15 -90 16 -88 19 -79 20 -64 21 -8 22 -
f2 0 32 -24 1 -0.9 0.9
;i1 0 2.756 1 1 0
;i1 3 2.756 1 -1 0
;i1 6 1.378 1 .5 2.067
 e
</CsScore>
</CsoundSynthesizer>
