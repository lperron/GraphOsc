# GraphOsc

GraphOsc is an oscillator wich generates waveforms from ODS graph.

## Dependencies

cpp csound libreoffice

## Installation & Compilation

```
~$ git clone https://gitlab.com/lperron/GraphOsc.git
~$ cd GraphOsc/C++/
~$ g++ ./graphosc.cpp -o ./graphosc
```

## Example

Here's a graph :

<p align='center'><img src='https://gitlab.com/lperron/GraphOsc/raw/master/pic/Cac40graph.png'</p>

Here's the wave !

<p align='center'><img src='https://gitlab.com/lperron/GraphOsc/raw/master/pic/Cac40wave.png'</p>







